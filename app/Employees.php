<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $fillable = [
        'saloon_id','first_name','last_name','address','phone','email','image','status','available',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    public function saloon_master()
    {
        return $this->belongsTo('App\saloon', 'saloon_id');
    }

}
