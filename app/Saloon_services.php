<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Saloon_services extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'saloon_id','type_id','charges','duration','avail_discount','discount','status',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    public function saloon()
    {
        return $this->belongsTo('App\saloon', 'id');
    }

    public function services_master()
    {
        return $this->belongsTo('App\Service_type', 'id');
    }


}
