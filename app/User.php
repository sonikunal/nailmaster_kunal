<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    protected $fillable = [
        'name','email','country','state_id','city_id','landmark','addressline_1','addressline_2','zip_code','phone','password','status','role','image','session_token',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /*For Status*/
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    const ROLE_SALOON = 'sub_admin';
    const ROLE_USER   = 'user';

    public static $user_role = [
        self::ROLE_SALOON => 'Saloon Owner',
        self::ROLE_USER => 'Normal User',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /*For Role*/

    /*For Display in Form*/
    public static $access = [
        self::Access_Admin => 'Admin',
        self::Access_subadmin => 'sub_admin',
        self::Access_user => 'User',
    ];

    /*For Entry in Database*/
    const Access_Admin = 'admin';
    const Access_subadmin = 'sub_admin';
    const Access_user = 'User';

    public function saloons()
    {
        return $this->hasOne('App\saloon','user_id');
    }

    public static function boot()
    {
        static::deleted(function($model) {
            foreach ($model->saloons as $saloon)
                $saloon->forceDelete();
        });
        parent::boot();
    }

}
