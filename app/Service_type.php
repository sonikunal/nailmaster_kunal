<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_type extends Model
{
	 protected $fillable = [
        'title','min_duration','status','displayorder',
    ];
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    public function services_types()
    {
        return $this->hasMany('App\Saloon_services','type_id');
    }


}
