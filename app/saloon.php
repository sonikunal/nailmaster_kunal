<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class saloon extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title','status','displayorder','description','location','user_id','latitude','longitude','start_hour','end_hour','image','global_discount'
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    const WORK_MONDAY = 'monday';
    const WORK_TUESDAY = 'tuesday';
    const WORK_WEDNESDAY = 'wedndesday';
    const WORK_THURSDAY = 'thursday';
    const WORK_FRIDAY = 'friday';
    const WORK_SATURDAY = 'saturday';
    const WORK_SUNDAY = 'sunday';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    public static $global_discount = [
        0 => 'No',
        1 => 'Yes',
    ];

    public static $work_days = [
        self::WORK_MONDAY => 'Monday',
        self::WORK_TUESDAY => 'Tuesday',
        self::WORK_WEDNESDAY => 'Wednesday',
        self::WORK_THURSDAY => 'Thursday',
        self::WORK_FRIDAY => 'Friday',
        self::WORK_SATURDAY => 'Saturday',
        self::WORK_SUNDAY => 'Sunday',
    ];

    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function Saloon_services()
    {
        return $this->hasMany('App\Saloon_services','saloon_id');
    }

    public function Work_days()
    {
        return $this->hasMany('App\work_days','saloon_id');
    }

    public function Employees()
    {
        return $this->hasMany('App\Employees','saloon_id');
    }

    public static function boot()
    {
        parent::boot();

        self::deleting(function($model) {

            foreach ($model->Saloon_services as $services)
                $services->delete();

            foreach ($model->work_days as $work_day)
                $work_day->delete();

            foreach ($model->Employees as $employee)
                $employee->delete();

        });
    }
}
