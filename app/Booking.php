<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'user_id','type_id','saloon_id','date','time','status',
    ];

    const WAITING = 'waiting';
    const IN_PROGRESS = 'in-progress';
    const CANSEL = 'cancel';
    const COMPLETED = 'completed';

    public static $status = [
        self::WAITING => 'waiting',
        self::IN_PROGRESS => 'in-progress',
        self::CANSEL => 'cancel',
        self::COMPLETED => 'completed',
    ];


    public function Saloon()
    {
        return $this->belongsTo('App\Saloon','saloon_id');
    }
      public function Type()
    {
        return $this->belongsTo('App\Service_type', 'type_id');
    }
}
