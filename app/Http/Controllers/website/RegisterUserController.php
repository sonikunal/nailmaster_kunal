<?php

namespace App\Http\Controllers\website;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterUserController extends Controller
{
    public function index()
    {
        return view('website.register');
    }

    public function register_user(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
        ]);


        return $request->all();

        exit;
    }
}
