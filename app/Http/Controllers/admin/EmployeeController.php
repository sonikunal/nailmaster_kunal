<?php

namespace App\Http\Controllers\admin;

use App\Employees;
use App\saloon;
use auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
    }

    public function index(Request $request)
    {
        $data['menu']="Saloon Employees";

        if(Auth::user()->role == 'admin')
        {

            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);

                $data['saloon'] = Employees::where($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('displayorder', 'asc')->Paginate($this->pagination);

                $data['search']=$request['search'];

            }
            else
            {
                $data['employees'] = Employees::with('saloon_master')->orderBy('id','asc')->Paginate($this->pagination);
            }

        }
        else
        {

            $saloon = saloon::select('id')->where('user_id', Auth::user()->id)->get();
            $saloon_id = $saloon[0]->id;

            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);

                $type=$request['type'];
                $search=$request['search'];

                $data['employees'] = Employees::where('saloon_id',$saloon_id)
                    ->where(function ($query) use ($search,$type) {
                        $query->where($type,$search);

                    })->Paginate($this->pagination);

                $data['search']=$request['search'];

            }
            else
            {

                $data['employees'] = Employees::where('saloon_id',$saloon_id)->orderBy('id','asc')->Paginate($this->pagination);

            }
        }

        return view('admin.employees.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['menu'] = "Saloon Employees";

        if(Auth::user()->role == 'admin') {
            $data['saloons'] = saloon::pluck('title', 'id')->all();
        }

        return view("admin.employees.create",$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'first_name' => 'required',

            'last_name' => 'required',

            'email' => 'required',

            'phone' => 'required',

            'status'=>'required',

            'image'=>'required|mimes:jpeg,bmp,png',

        ]);

        $input = $request->all();

        if(Auth::user()->role == 'sub_admin') {
            $saloon = saloon::select('id')->where('user_id', Auth::user()->id)->get();
            $input['saloon_id'] = $saloon[0]->id;
        }

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'employees');
        }

        Employees::create($input);

        \Session::flash('success', 'Employee has been added successfully!');

        return redirect('admin/employees');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [];
        $data['menu'] = "Saloon Employees";
        $data['employee'] = Employees::findOrFail($id);

        if(Auth::user()->role == 'admin') {
            $data['saloons'] = saloon::pluck('title', 'id')->all();
        }

        return view("admin.employees.edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'first_name' => 'required',

            'last_name' => 'required',

            'email' => 'required',

            'phone' => 'required',

            'status'=>'required',

            'image'=>'required|mimes:jpeg,bmp,png',

        ]);

        $input = $request->all();

        $employee = Employees::findOrFail($id);

        if($photo = $request->file('image'))
        {

            if(file_exists($employee->image))
            {
                unlink($employee->image);
            }
            $input['image'] = $this->image($photo,'saloon');

        }

        $employee->update($input);

        \Session::flash('success','Employee has been updated successfully!');
        return redirect('admin/employees');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employees::findOrFail($id);
        $employee->delete();
        \Session::flash('danger','Employee has been deleted successfully!');
        return redirect()->back();
    }

    public function assign(Request $request)
    {
        $employee = Employees::findorFail($request['id']);
        $employee['status'] = "active";
        $employee->update($request->all());
        return $request['id'];
    }
    public function unassign(Request $request)
    {
        $employee = Employees::findorFail($request['id']);
        $employee['status'] = "in-active";
        $employee->update($request->all());
        return $request['id'];
    }

}
