<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class ProfileupdateController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
        //$this->middleware('user_check:'.$id);
    }

    public function edit($id)
    {
        $data=[];
        $data['menu']="User";
        $data['user'] = User::findorFail($id);
        return view('admin.users.profile_edit',$data);
    }


    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',

            'email' => 'required|email|unique:users,email,'.$id.',id',

            'password' => 'confirmed',

            'country' => 'required',
            
            'phone' => 'required',

            'image'=>'mimes:jpeg,bmp,png',

        ]);

        if(!empty($request['password'])){
        }
        else{
            unset($request['password']);
        }

        $input = $request->all();
        $user = User::findorFail($id);  
        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'user');
            if(file_exists($user->image))
            {
                unlink($user->image);
            }
        }
        $user->update($input);
        \Session::flash('success','User has been updated successfully!');
        return redirect('admin/profile_update/'.$id."/edit");
    }
}
