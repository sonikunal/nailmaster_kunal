<?php

namespace App\Http\Controllers\admin;

use App\Service_type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('role');
    }
    
    public function index(Request $request)
    {
        $data['menu']="Service Type";

        if(isset($request['search']) && $request['search'] != '')
        {
            $this->validate($request, [
                'type' => 'required',
            ]);
            
            $data['service'] = Service_type::where($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('displayorder', 'asc')->Paginate($this->pagination);
            $data['search']=$request['search'];
            

        }
        else
        {
            $data['service']=Service_type::orderBy('displayorder','asc')->Paginate($this->pagination);
 
        }
        if ($request->ajax()) 
        {
            return view('admin.service.table',$data);
        }
        
        return view('admin.service.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $data['menu'] = 'Service Type';
        return view('admin.service.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'min_duration' => 'required',
            'status' => 'required',
            
        ]);
        $input = $request->all();
        Service_type::create($input);

        \Session::flash('success', 'Service type has been inserted successfully!');
        return redirect('admin/service');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service_type  $service_type
     * @return \Illuminate\Http\Response
     */
    public function show(Service_type $service_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service_type  $service_type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = 'Service Type ';
        $data['service'] = Service_type::findOrFail($id);
         return view('admin.service.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service_type  $service_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service_type $service_type,$id)
    {
        $this->validate($request, [
            'title' => 'required',
            'min_duration' => 'required',
            'status' => 'required',
        ]);
         $service_type = Service_type::findOrFail($id);
         $input = $request->all();
         $service_type->update($input);

        \Session::flash('success','Service type has been updated successfully!');
        return redirect('admin/service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service_type  $service_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service_type = Service_type::findOrFail($id);
        $service_type->delete();
        \Session::flash('danger','Service type has been deleted successfully!');
        return redirect()->back();
    }
    public function assign(Request $request)
    {
        $service_type = Service_type::findorFail($request['id']);
        $service_type['status'] = "active";
        $service_type->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $service_type = Service_type::findorFail($request['id']);
        $service_type['status'] = "in-active";
        $service_type->update($request->all());
        return $request['id'];
    }
      public function reorder(Request $request){
        $array = $_GET['arrayorder'];
        if ($_GET['update'] == "update") {
            $count = 1;
            foreach ($array as $idval) {

                $service = Service_type::findOrFail($idval);
                $input['displayorder'] = $count;
                $service->update($input);
                $count ++;
            }
            echo 'Display order change successfully.';
        }
    }

}
