<?php

namespace App\Http\Controllers\admin;

use App\AppUser;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('accessright');
    }

    public function index()
    {
        $data['mainmenu'] = "";
        $data['menu'] = "Dashboard";
        if(Auth::user()->role == 'admin')
        {
            $data['total_user'] = User::where('role', '!=', 'admin')->count();
        }
      

        $data['amount_chart_labels'] ="";
        $data['amount_chart_data'] = "";

        return view('admin.dashboard',$data);
    }
}
