<?php

namespace App\Http\Controllers\admin;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use auth;
class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
    }
    public function index(Request $request)
    {
        $data['menu']="Booking";
        if(Auth::user()->role == 'admin')
        {

            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);
                
                $data['booking'] = Booking::where($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('id', 'desc')->Paginate($this->pagination);
                $data['search']=$request['search'];
                

            }
            else
            {

                $data['booking']=Booking::orderBy('id','desc')->Paginate($this->pagination);
            }

        }
        else
        {
            $user_id = Auth::user()->id;
            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);
                
                $type=$request['type'];
                $search=$request['search'];

                $data['booking'] = Booking::where('user_id',$user_id)
                ->where(function ($query) use ($search,$type) {
                    $query->where($type,$search);

                })->Paginate($this->pagination);

                $data['search']=$request['search'];
            
            }
            else
            {

                $data['booking']=Booking::where('user_id',$user_id)->orderBy('id','desc')->Paginate($this->pagination);

            }
        }
        return view('admin.booking.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::findOrFail($id);
        $booking->delete();
        \Session::flash('danger','Booking  has been deleted successfully!');
        return redirect()->back();
    }
    public function update_status($statusval,$id)
    {
        $status = Booking::findOrFail($id);
        $input['status'] = $statusval;
        $status->update($input);
        return 'success';
    }
}
