<?php

namespace App\Http\Controllers\admin;

use App\saloon;
use App\Saloon_services;
use App\Service_type;
use App\User;
use App\work_days;
use auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaloonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('accessright');
    }
    public function index(Request $request)
    {
        $data['menu']="Saloon";
        if(Auth::user()->role == 'admin')
        {

            if(isset($request['search']) && $request['search'] != '')
            {
                $this->validate($request, [
                    'type' => 'required',
                ]);
                
                $data['saloon'] = saloon::with('saloon_owner')->where($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('displayorder', 'asc')->Paginate($this->pagination);

                $data['search']=$request['search'];

            }
            else
            {
                $data['saloon'] = saloon::with('User')->orderBy('displayorder','asc')->Paginate($this->pagination);
            }

            return view('admin.saloon.index',$data);
        }
        else
        {
            $user_id = Auth::user()->id;

            $data['saloon']=saloon::where('user_id',$user_id)->orderBy('displayorder','asc')->first();

            if(isset($data['saloon'])) {
                if($data['saloon']->id) {

                    $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
                    $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
                    $data['work_days'] = work_days::select('work_day')->where('saloon_id', $data['saloon']->id)->get();
                    $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $data['saloon']->id)->get();

                    foreach($service as $key => $value) {
                        $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'] == 1 ? 'Yes' : 'No', 'discount' => $value['discount']);
                    }

                }
            } else {
                return redirect('admin/saloon/create');
            }

            return view('admin.saloon.view',$data);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function create()
    {
        $data = [];
        $data['menu'] = "Saloon";
        $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();

        if(Auth::user()->role == 'admin') {
            $data['saloon_users'] = User::where('role','sub_admin')->pluck('email', 'id')->all();
        }

        return view('admin.saloon.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',

            'latitude' => 'required',

            'longitude' => 'required',
            
            'location' => 'required',

            'description' => 'required',

            'start_hour' => 'required',

            'end_hour' => 'required',

            'status'=>'required',

            'image'=>'required|mimes:jpeg,bmp,png',

        ]);

        $input = $request->all();

        if(Auth::user()->role != 'admin') {
            $input['user_id'] = Auth::user()->id;
        }

        if($photo = $request->file('image'))
        {
            $input['image'] = $this->image($photo,'saloon');
        }

        $saloon = saloon::create($input);

        \Session::flash('success', 'Saloon has been inserted successfully!');

        return redirect('admin/saloon/services/'.$saloon->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\saloon  $saloon
     * @return \Illuminate\Http\Response
     */
    public function show(saloon $saloon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\saloon  $saloon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = 'Saloon ';
        $data['saloon'] = saloon::findOrFail($id);

        $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
        $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();

        if(Auth::user()->role == 'admin') {
            $data['saloon_users'] = User::where('role','sub_admin')->pluck('email', 'id')->all();
        }

        $data['service'] = Saloon_services::select('charges', 'duration', 'type_id')->where('saloon_id', $id)->get();

        foreach($data['service'] as $key => $value) {
            $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration']);
        }

        return view('admin.saloon.edit',$data);
    }

    public function services($id)
    {
        $data['menu'] = 'Saloon ';
        $data['saloon'] = saloon::findOrFail($id);

        $data['type'] = Service_type::where('status','active')->pluck('title','id')->all();
        $data['duration'] = Service_type::where('status','active')->pluck('min_duration','id')->all();
        $service = Saloon_services::select('charges', 'duration', 'type_id', 'avail_discount', 'discount')->where('saloon_id', $id)->get();
        $data['avail_discount'] = array(0 => 'No',  1 => 'Yes');
        $data['discount'] = array(0 => 0, 5 => 5, 10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50);

        foreach($service as $key => $value) {
            $data['services'][$value['type_id']] = array('charges' => $value['charges'], 'duration' => $value['duration'], 'avail_discount' => $value['avail_discount'], 'discount' => $value['discount']);
        }

        return view('admin.saloon.services',$data);
    }

    public function save_services(Request $request, $id)
    {

        $input = $request->all();

        $services = Saloon_services::where('saloon_id', $id);

        if(! isset($input['type_id'])) {
            \Session::flash('error', 'You have to select at least one service.');
            return redirect()->back();
        }

        if($services) {
            $services->forceDelete();
        }

        foreach($input['type_id'] as $tid) {

            $service['saloon_id']       = $id;
            $service['type_id']         = $tid;
            $service['charges']         = $input['charges_'.$tid];
            $service['duration']        = $input['duration_'.$tid];
            $service['avail_discount']  = $input['avail_discount_'.$tid];
            $service['discount']        = $input['discount_'.$tid];
            $service['status']          = 'active';
            Saloon_services::create($service);

        }

        \Session::flash('success', 'Saloon details have been updated successfully!');

        return redirect('admin/saloon/work_days/'.$id);

    }

    public function work_days($id)
    {
        $data['menu'] = 'Saloon ';
        $data['saloon'] = saloon::findOrFail($id);

        $data['working_days'] = work_days::where('saloon_id', $id)->pluck('work_day')->all();

        return view('admin.saloon.work_days',$data);
    }

    public function save_days(Request $request, $id)
    {

        $input = $request->all();

        $working_days = work_days::where('saloon_id', $id);

        if(! isset($input['work_days'])) {
            \Session::flash('error', 'You have to select at least one working day.');
            return redirect()->back();
        }

        if($working_days) {
            $working_days->forceDelete();
        }

        foreach($input['work_days'] as $day) {

            $wdays['saloon_id'] = $id;
            $wdays['work_day']  = $day;

            work_days::create($wdays);

        }

        \Session::flash('success', 'Saloon details have been updated successfully!');

        return redirect('admin/saloon');

    }

    public function images($id)
    {
        $data['menu'] = 'Saloon ';
        $data['saloon'] = saloon::findOrFail($id);
        return view('admin.saloon.images',$data);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\saloon  $saloon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'title' => 'required',

            'latitude' => 'required',

            'longitude' => 'required',
            
            'location' => 'required',

            'description' => 'required',

            'start_hour' => 'required',

            'end_hour' => 'required',

            'status'=>'required',

            'image'=>'mimes:jpeg,bmp,png',

        ]);

        $input = $request->all();

        $saloondata = saloon::findOrFail($id);

        if($photo = $request->file('image'))
        {

            if(file_exists($saloondata->image))
            {
                unlink($saloondata->image);
            }
            $input['image'] = $this->image($photo,'saloon');

        }

        $saloondata->update($input);

        // \Session::flash('success','Saloon type has been updated successfully!');

        return redirect('admin/saloon/services/'.$id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\saloon  $saloon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $saloon = saloon::findOrFail($id);
        $saloon->delete();
        \Session::flash('danger','Saloon  has been deleted successfully!');
        return redirect()->back();
    }
    public function assign(Request $request)
    {
        $saloon = saloon::findorFail($request['id']);
        $saloon['status'] = "active";
        $saloon->update($request->all());
        return $request['id'];
    }
    public function unassign(Request $request)
    {
        $saloon = saloon::findorFail($request['id']);
        $saloon['status'] = "in-active";
        $saloon->update($request->all());
        return $request['id'];
    }
      public function reorder(Request $request){
        $array = $_GET['arrayorder'];
        if ($_GET['update'] == "update") {
            $count = 1;
            foreach ($array as $idval) {

                $saloon = saloon::findOrFail($idval);
                $input['displayorder'] = $count;
                $saloon->update($input);
                $count ++;
            }
            echo 'Display order change successfully.';
        }
    }
}
