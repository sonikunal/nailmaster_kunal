<?php



namespace App\Http\Controllers\admin;

use App\Categories;

use App\cities;
use App\countries;
use App\states;
use App\User;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class UserController extends Controller

{

    public function __construct(Request $request)
    {

        $this->middleware('auth');

        $this->middleware('role');

    }

    public function index(Request $request)
    {

        $data = [];

        $data['menu'] = "User";

        if (isset($request['search']) && $request['search'] != '') {
            $this->validate($request, [
                'type' => 'required',
            ]);
            $search = $request['search'];
            $type = $request['type'];
            $data['user'] = User::where('role', '!=', 'admin')
                ->where(function ($query) use ($search, $type) {
                    $query->where($type, $search);
                })->Paginate($this->pagination);

            // $data['user'] = User::orWhere($request['type'], 'like', '%'.$request['search'].'%')->OrderBy('id', 'desc')->Paginate($this->pagination);


            $data['search'] = $request['search'];


        } else {
            $data['user'] = User::orderBy('id', 'desc')->where('role', '!=', 'admin')->Paginate($this->pagination);
        }

        //$data['user'] = User::where('role','!=','admin')->get();

        foreach ($data['user'] as $key => $val) {

            $cc = $val['country'];

            $cdata = countries::select('countryName')->where('countryID', $cc)->first();

            $data['user'][$key]['country'] = $cdata['countryName'];

        }

        return view('admin.users.index', $data);

    }

    public function create()

    {

        $data = [];

        $data['menu'] = "User";

        $data['countries'] = countries::pluck('countryName', 'countryID')->all();

        $data['states'] = [];

        $data['cities'] = [];

        //$data['dases'] = '';

        return view("admin.users.create", $data);

    }

    public function store(Request $request)

    {

        $this->validate($request, [

            'name' => 'required',

            'email' => 'required|email|unique:users,email',

            'password' => 'required',

            'country' => 'required',

            'state_id' => 'required',

            'city_id' => 'required',

            'addressline_1' => 'required',

            'zip_code' => 'required',

            'phone' => 'required',

            'status' => 'required',

            // 'image' => 'required|mimes:jpeg,bmp,png',

        ]);

        $input = $request->all();

        if ($photo = $request->file('image')) {

            $input['image'] = $this->image($photo, 'User');

        }

        if (!empty($request['category'])) {

            $input['cat_id'] = implode(',', $request['category']);
        }

        User::create($input);

        \Session::flash('success', 'User has been inserted successfully!');

        return redirect('admin/users');

    }

    public function show($id)

    {

        //

    }

    public function edit($id)

    {

        $data = [];

        $data['menu'] = "User";

        $data['user'] = User::findorFail($id);

        $data['countries'] = countries::pluck('countryName', 'countryID')->all();

        $data['states'] = states::where('countryID', $data['user']->country)
            ->orderBy('stateName', 'ASC')->pluck('stateName', 'stateID')->all();

        $data['cities'] = cities::where('countryID', $data['user']->country)->where('stateID', $data['user']->state_id)
            ->orderBy('cityName', 'ASC')->pluck('cityName', 'cityID')->all();

        return view('admin.users.edit', $data);

    }

    public function update(Request $request, $id)

    {

        $this->validate($request, [

            'name' => 'required',

            'email' => 'required|email|unique:users,email,' . $id . ',id',

            'status' => 'required',

            'country' => 'required',

            'state_id' => 'required',

            'city_id' => 'required',

            'addressline_1' => 'required',

            'zip_code' => 'required',

            'phone' => 'required',

        ]);

        $input = $request->all();

        $user = User::findorFail($id);

        if (!empty($request['password'])) {

        } else {

            unset($request['password']);

        }

        if (!empty($request['image'] && $request['image'] != "")) {

            if (file_exists($user->image))

                unlink($user->image);

        }

        if ($photo = $request->file('image')) {

            $input['image'] = $this->image($photo, 'user');

        }

        $user->update($input);

        \Session::flash('success', 'User has been updated successfully!');

        return redirect('admin/users');

    }


    public function destroy($id)

    {

        $appuser = User::findOrFail($id);

        $appuser->forcedelete();

        \Session::flash('danger', 'User has been deleted successfully!');

        return redirect('admin/users');

    }

    public function assign(Request $request)
    {

        $appuser = User::findorFail($request['id']);

        $appuser['status'] = "active";

        $appuser->update($request->all());

        return $request['id'];

    }

    public function unassign(Request $request)
    {

        $appuser = User::findorFail($request['id']);

        $appuser['status'] = "in-active";

        $appuser->update($request->all());

        return $request['id'];

    }

    public function get_states($cc)
    {

        $states = states::select('stateName', 'stateID')->where('countryID', $cc)
            ->orderBy('stateName', 'ASC')->get();

        $options = '';

        foreach ($states as $state) {
            $options .= '<option value="' . $state['stateID'] . '">' . $state['stateName'] . '</option>';

        }

        echo $options;
        return '';

    }

    public function get_cities($cc, $st)
    {

        $cities = cities::select('cityName', 'cityID')->where('countryID', $cc)->where('stateID', $st)
            ->orderBy('cityName', 'ASC')->get();

        $options = '';

        foreach ($cities as $city) {
            $options .= '<option value="' . $city['cityID'] . '">' . $city['cityName'] . '</option>';

        }

        echo $options;
        return '';

    }
}