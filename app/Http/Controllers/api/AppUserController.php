<?php

namespace App\Http\Controllers\api;

use App\saloon;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class AppUserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("email", $request['email'])->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Email address or password is incorrect. Please try again.";
                return response($response, 200);
            }
            else {

                if( Hash::check($request['password'],$appuser['password']) == false)
                {
                    $response["Result"] = 0;
                    $response["Message"] = "Email address or password is incorrect. Please try again.";
                    return response($response, 200);
                }
                else
                {
                    $appuser['remember_token'] = str_random(30);
                    $img = url('assets/dist/img/default.png');
                    $appuser->save();
                    $user = User::findorFail($appuser->id);
                    if($user->image == '')
                    {
                        $user->image = $img;
                    }

                    unset($user->password);
                    unset($user->deleted_at);

                    if ($user->image != "" && file_exists($user['image'])) {
                        $user->image = url($user->image);
                    }

                    $user['token'] = $appuser['remember_token'];

                    $response["Result"] = 1;
                    $response["User"] = $user;
                    $response['Message'] = "User login Successfully.";
                    return response($response, 200);
                }

            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'name'  => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            else {

                $input = $request->all();
                $input['role'] = 'user';
                $input['remember_token'] = str_random(30);

                $img = url('assets/dist/img/default.png');

                if($photo = $request->file('image'))
                {
                    $input['image'] = url($this->image($photo,'User'));
                }

                $appuser = User::create($input);

                $user = User::findorFail($appuser->id);
                if($user->image == '')
                {
                    $user->image = $img;
                }


                array_walk_recursive($user, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                unset($user->deleted_at);
                unset($user->password);

                $response["Result"] = 1;
                $response["User"] = $user;
                $response['Message'] = "User Registered Successfully.";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_profile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("remember_token", $request['remember_token'])->first();
            if (empty($appuser) || $request['remember_token'] == "") {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 401);
            }

            if (!empty($appuser)) {

                unset($appuser->password);
                unset($appuser->deleted_at);
                $img = url('assets/dist/img/default.png');
                if($appuser->image == '')
                {
                    $appuser->image = $img ;
                }

                array_walk_recursive($appuser, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function profile_update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("remember_token", $request['remember_token'])->first();
            if (empty($appuser) || $request['remember_token'] == "") {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 401);
            }

            if (!empty($appuser)) {

                $input = $request->all();

                $img = url('assets/dist/img/default.png');

                if(empty($request['password'])){
                    unset($input['password']);
                }

                if($photo = $request->file('image'))
                {
                    $input['image'] = url($this->image($photo,'User'));
                }

                array_walk_recursive($input, function (&$item, $key) {
                    $item = null === $item ? '' : $item;
                });

                $appuser->update($input);
                if($appuser->image == '')
                {
                    $appuser->image = $img;
                }

                unset($appuser->password);
                unset($appuser->deleted_at);

                $response["Result"] = 1;
                $response["User"] = $appuser;
                $response['Message'] = "User Profile Updated Successfully";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function forgot_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("email", $request['email'])->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Invalid email address.";
                return response($response, 200);
            }
            else {
                $pass=str_random(8);
                $appuser->password=$pass;
                $appuser->save();

                $to1=$request['email'];
                $from1='noreply@eshoppers.com';
                $subject2='Forget Password';
                $mailcontent1="Dear <b>" .$appuser->name."</b>, You have requested to reset your password. Please use the password <b>" .$pass. "</b> to log in. After log in, please go to Profile page to change your password.";
                $headers  = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "From: $from1\r\n";
                mail($to1,$subject2,$mailcontent1,$headers);
                $response["Result"]=1;
                $response["Message"] = "A new password has been sent to your registered email address.";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_saloons(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'remember_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = User::where("remember_token", $request['remember_token'])->first();
            if (empty($appuser) || $request['remember_token'] == "") {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 401);
            }

            if (!empty($appuser)) {

                date_default_timezone_set('Asia/Kolkata');

                $date_time = Carbon::now();
                $today = strtolower($date_time->format('l'));

                $saloons = saloon::with('services')->with('work_days')->with('employees')->orderBy('displayorder','asc')->get();

                // select('title', 'location', 'latitude', 'longitude', 'image')->

                foreach($saloons as $key => $val) {

                    $status = 'closed';
                    $map_pin = url('assets/dist/img/icons/close.png');

                    $open_time = Carbon::createFromTimeString($val['start_hour']);
                    $close_time = Carbon::createFromTimeString($val['end_hour']);

                    if(count($val['work_days'])) {

                        foreach($val['work_days'] as $wd) {
                            if($wd['work_day'] == $today) {
                                $status = 'open';
                                break;
                            }
                        }

                    }

                    if($status == 'open') {

                        if($date_time->gte($open_time) && $date_time->lte($close_time)) {
                            $status = 'open';
                        } else {
                            $status = 'close';
                        }

                    }

                    if(count($val['employees'])) {

                        foreach($val['employees'] as $key1 => $val1) {
                            $val['employees'][$key1]['image'] = url($val1['image']);
                        }

                    }

                    $saloons[$key]['image'] = url($val['image']);

                    $saloons[$key]['status'] = $status;

                    if($status == 'open') {
                        $map_pin = url('assets/dist/img/icons/15_min.png');
                    }

                    $saloons[$key]['map_pin'] = $map_pin;

                }

                $response["Result"] = 1;
                $response["saloons"] = $saloons;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }


}
