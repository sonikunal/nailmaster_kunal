@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper">

        <section class="content-header">

            <h1>

                {{$menu}}

            </h1>

            <ol class="breadcrumb">

                <li><a href="{{ url('admin/saloon') }}"><i class="fa fa-dashboard"></i> Booking</a></li>

            </ol>

        </section>

        <section class="content">

            @include ('admin.error')

            <div id="responce" name="responce" class="alert alert-success" style="display: none">

            </div>



            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/booking'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-sm-5 col-xs-12" style="padding-top: 5px">
                                <select name="type" style="width: 100%" class="select2 form-control">
                                    <option value="">Please Select</option>
                                    
                                    <option value="date" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='date') selected="selected" @endif>Date</option>

                                    <option value="time" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='time') selected="selected" @endif>Time</option>

                                    <option value="status" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='status') selected="selected" @endif>Status</option>
                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">
                
                        <a href="{{ url('admin/booking') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>

                    </h3>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive">

                    <table class="table table-bordered table-striped" id="example2">

                        <thead>

                        <tr>
                            <th>Id</th>
                            <th>Type </th>
                            <th>Saloon</th>
                            <th>Date</th>
                            <th>Time</th>    
                            <th>Status</th>
                            <th>Delete</th>

                        </tr>

                        </thead>

                        <tbody id="sortable">

                        @foreach ($booking as $list)

                            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">


                                <td>{{ $list['id'] }}</td>

                                <td>{{$list->Type->title}}</td>

                                <td>{{$list->Saloon->title}}</td>

                                <td>{{$list['date']}}</td>

                                <td>{{$list['time']}}</td>

                                <td>
                                  
                                    <?php $status_selected =$list['status']; ?>
                                    {!! Form::select('status', \App\Booking::$status, !empty($status_selected)?$status_selected:null,['class' => 'select2 select2-hidden-accessible form-control', 'style' => 'width: 100%','onchange'=> 'Statuschange(this.value,'.$list->id.');']) !!}
                                </td>



                                </td>

                                <td>

                                    <div class="btn-group-horizontal">

                                        <span data-toggle="tooltip" title="Delete booking" data-trigger="hover">

                                            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>

                                        </span>


                                    </div>

                                </td>

                            </tr>

                            <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">

                                {{ Form::open(array('url' => 'admin/booking/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                                <span aria-hidden="true">&times;</span></button>

                                            <h4 class="modal-title">Delete Booking</h4>

                                        </div>

                                        <div class="modal-body">

                                            <p>Are you sure you want to delete this booking ?</p>

                                        </div>

                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

                                            <button type="submit" class="btn btn-outline">Delete</button>

                                        </div>

                                    </div>

                                </div>

                                {{ Form::close() }}

                            </div>

                        @endforeach

                    </table>
                    <div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $booking])</div>

                </div>

            </div>

        </section>

    </div>

@endsection



<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">

<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>

<script>Ladda.bind( 'input[type=submit]' );</script>

<script type="text/javascript">

    function Statuschange(val,id){

        if(val == '') val = 0;
        $.ajax({
            url: '{{ url('admin/booking') }}/'+val +'/'+ id,
            error:function(){
            },
            success: function(result){

            }
        });
    }

</script>