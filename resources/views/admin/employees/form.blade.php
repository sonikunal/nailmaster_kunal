{!! Form::hidden('redirects_to', URL::previous()) !!}

@if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')

    <div class="form-group{{ $errors->has('saloon_id') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="saloon_id">Saloon<span class="text-red">*</span></label>

        <div class="col-sm-5">

            {!! Form::select('saloon_id', $saloons, null, ['id'=>'saloon_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
            @if ($errors->has('saloon_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('saloon_id') }}</strong>
                </span>
            @endif

        </div>

    </div>


@endif

<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">First Name <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}

        @if ($errors->has('first_name'))

            <span class="help-block">

                <strong>{{ $errors->first('first_name') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Last Name <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}

        @if ($errors->has('last_name'))

            <span class="help-block">

                <strong>{{ $errors->first('last_name') }}</strong>

            </span>

        @endif

    </div>

</div>



<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="email">Email <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) !!}

            @if ($errors->has('email'))

            <span class="help-block">

            <strong>{{ $errors->first('email') }}</strong>

            </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Phone <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::number('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone']) !!}

        @if ($errors->has('phone'))

            <span class="help-block">

                <strong>{{ $errors->first('phone') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Address </label>

    <div class="col-sm-5">

        {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) !!}

        @if ($errors->has('address'))

            <span class="help-block">

                <strong>{{ $errors->first('address') }}</strong>

            </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="image">Image<span class="text-red">*</span></label>

    <div class="col-sm-5">

        <div class="">

            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}

        </div>

        <?php

        if (!empty($employee->image) && $employee->image != "") {

        ?>

        <br><img id="DisplayImage" src="{{ url($employee->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >

        <?php

        }else{

            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';

        } ?>



        @if ($errors->has('image'))

            <span class="help-block">

                    <strong>{{ $errors->first('image') }}</strong>

                </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>

    <div class="col-sm-5">

        @foreach (\App\Employees::$status as $key => $value)

            <label>

                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>

            </label>

        @endforeach



        @if ($errors->has('status'))

            <span class="help-block">

             <strong>{{ $errors->first('status') }}</strong>

            </span>

        @endif

    </div>

</div>


<script type="text/javascript">

    $("#image").fileinput({

        showUpload: false,

        showCaption: false,

        showPreview: false,

        showRemove: false,

        browseClass: "btn btn-primary btn-lg btn_new",

    });



    function AjaxUploadImage(obj,id){

        var file = obj.files[0];

        var imagefile = file.type;

        var match = ["image/jpeg", "image/png", "image/jpg"];

        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))

        {

            $('#previewing'+URL).attr('src', 'noimage.png');

            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            return false;

        } else{

            var reader = new FileReader();

            reader.onload = imageIsLoaded;

            reader.readAsDataURL(obj.files[0]);

        }

    }

    function imageIsLoaded(e) {

        $('#DisplayImage').css("display", "block");

        $('#DisplayImage').attr('src', e.target.result);

        $('#DisplayImage').attr('width', '150');

    };

</script>