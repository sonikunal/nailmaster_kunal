@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">

        <section class="content-header">
            <h1>
                Saloon
                <small>View</small>
            </h1>
        </section>


        <section class="content">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#saloonManagement" data-toggle="tab" aria-expanded="true">Saloon Details
                            Management</a></li>
                    @if (isset($saloon->id))
                        <li class=""><a href="#servicesManagement" data-toggle="tab" aria-expanded="false">Service Details</a></li>
                        <li class=""><a href="#workDays" data-toggle="tab" aria-expanded="false">Working Days</a></li>
                    @endif
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="saloonManagement">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">

                                    <div class="box-body">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <th>Saloon Name</th>
                                                <td>{{$saloon->title}}</td>
                                            </tr>
                                            <tr>
                                                <th>Location</th>
                                                <td>{{$saloon->location}}</td>
                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <td>{!! $saloon->description !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>{!!  $saloon['status']=='active'? '<span class="label label-success">Active</span>' : '<span class="label label-danger">In-active</span>' !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Created At</th>
                                                <td>{{$saloon->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <th>Working Hours</th>
                                                <td>{{ $saloon->start_hour .' - '. $saloon->end_hour }}</td>
                                            </tr>
                                            <tr>
                                                <th>Saloon Logo</th>
                                                <td>

                                                    <?php if (!empty($saloon->image) && $saloon->image != "") { ?>

                                                    <img id="DisplayImage" src="{{ url($saloon->image) }}" name="img"
                                                         id="img" width="150" style="padding-bottom:5px">

                                                    <?php } else { ?>

                                                    <img id="DisplayImage" src="" width="150" style="display: none;"/>

                                                    <?php } ?>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        {{ Form::open(array('url' => 'admin/saloon/'.$saloon['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}

                                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit Details" data-trigger="hover" type="submit" >Edit Details</button>

                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (isset($saloon->id))
                        <div class="tab-pane" id="servicesManagement">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box">

                                        <div class="box-body">

                                            <div class="alert {{ $saloon->global_discount == 1 ? 'alert-success' : 'alert-danger' }}">
                                                Global Discount : {{ $saloon->global_discount == 1 ? 'Yes' : 'No' }}
                                            </div>

                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th>Service Type</th>
                                                    <th>Charges (USD)</th>
                                                    <th>Duratin (Minutes)</th>
                                                    <th>Avail Discount</th>
                                                    <th>Discount (%)</th>
                                                </tr>

                                                @foreach ($type as $key => $value)

                                                    @if(isset($services[$key]))

                                                        <tr>
                                                            <td>
                                                                {{ $value }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['charges'] }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['duration'] }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['avail_discount'] }}
                                                            </td>

                                                            <td>
                                                                {{ $services[$key]['discount'] }}
                                                            </td>
                                                        </tr>

                                                    @endif

                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif

                    @if (isset($saloon->id))
                        <div class="tab-pane" id="workDays">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box">

                                        <div class="box-body">
                                            <table class="table table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th>Working Days</th>
                                                </tr>

                                                @foreach ($work_days as $day)

                                                    <tr>
                                                        <td>
                                                            {{ ucfirst($day['work_day']) }}
                                                        </td>

                                                    </tr>

                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif

                </div>
            </div>
        </section>
    </div>
@endsection
