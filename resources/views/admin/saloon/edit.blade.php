@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/service')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{$menu}}</h3>
                        </div>
                        {!! Form::model($saloon,['url' => url('admin/saloon/'.$saloon->id),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="nav-tabs-custom">
                            <div class="box-body">
                                @include ('admin.saloon.form')
                            </div>
                            <div class="box-footer">
                                <a href="{{ URL::previous() }}" ><button class="btn btn-default" type="button">Back</button></a>
                                <button class="btn btn-info pull-right" type="submit">Next</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection




