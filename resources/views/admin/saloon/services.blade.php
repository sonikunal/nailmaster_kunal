@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{$menu}}</h3>
                        </div>
                        {!! Form::model($saloon,['url' => url('admin/saloon/save_services/'.$saloon->id),'method'=>'put' ,'class' => 'form-horizontal']) !!}
                        <div class="box-body">
                            <div class="col-sm-12">

                                <div class="col-sm-3">
                                    <label class="control-label" for="address">Service Type</label>
                                </div>

                                <div class="col-sm-2">
                                    <label class="control-label" for="address">Charges (USD)</label>
                                </div>

                                <div class="col-sm-2">
                                    <label class="control-label" for="address">Duration (Minutes)</label>
                                </div>

                                <div class="col-sm-2">
                                    <label class="control-label" for="address">Avail Discount</label>
                                </div>

                                <div class="col-sm-2">
                                    <label class="control-label" for="address">Discount (%)</label>
                                </div>

                            @foreach ($type as $key => $value)

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <input type="checkbox" @if(isset($services[$key])) checked @endif name="type_id[]" value="{{ $key }}"> {{$value}}
                                            </div>

                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" name="charges_{{ $key }}" @if(isset($services[$key]['charges'])) value="{{$services[$key]['charges']}}" @else value="0.00" @endif placeholder="0.00">
                                            </div>

                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" name="duration_{{ $key }}" @if(isset($services[$key]['duration'])) value="{{$services[$key]['duration']}}" @else value="{{$duration[$key]}}" @endif placeholder="0.00">
                                            </div>

                                            <div class="col-sm-2">
                                                {!! Form::select('avail_discount_'.$key, $avail_discount, isset($services[$key]['avail_discount'])?$services[$key]['avail_discount']:null, ['class' => 'select2 form-control']) !!}
                                            </div>

                                            <div class="col-sm-2">
                                                {!! Form::select('discount_'.$key, $discount, isset($services[$key]['discount'])?$services[$key]['discount']:null, ['class' => 'select2 form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{ url('admin/saloon/'.$saloon->id.'/edit') }}"><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Next</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection