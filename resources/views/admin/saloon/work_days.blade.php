@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/saloon')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('success')}}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close">&times;</button>
                    {{Session::get('error')}}
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{$menu}}</h3>
                        </div>
                        {!! Form::model($saloon,['url' => url('admin/saloon/save_days/'.$saloon->id),'method'=>'put' ,'class' => 'form-horizontal']) !!}
                        <div class="box-body">
                            <div class="col-sm-12">

                                <div class="col-sm-4">
                                    <label class="control-label" for="address">Work Days</label>
                                </div>

                                <br/>
                                <br/>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <input type="checkbox" id="all_days"> All Days
                                        </div>
                                    </div>
                                </div>

                                @foreach (\App\saloon::$work_days as $day => $display)

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <input class="work_days" type="checkbox" @if(in_array($day, $working_days)) checked @endif name="work_days[]" value="{{ $day }}"> {{$display}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{ url('admin/saloon/services/'.$saloon->id) }}"><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Next</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('jquery')
    <script type="text/javascript">

        $('#all_days').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });

    </script>
@endsection