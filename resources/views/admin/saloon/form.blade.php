{!! Form::hidden('redirects_to', URL::previous()) !!}

    @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')

        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">

            <label class="col-sm-1 control-label" for="type_id">User<span class="text-red">*</span></label>

            <div class="col-sm-6">

                {!! Form::select('user_id', $saloon_users, null, ['id'=>'user_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
                @if ($errors->has('user_id'))
                    <span class="help-block">
            <strong>{{ $errors->first('user_id') }}</strong>
        </span>
                @endif

            </div>

        </div>

    @endif

    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="title">Saloon Name <span class="text-red">*</span></label>

        <div class="col-sm-6">

            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Saloon Name']) !!}

            @if ($errors->has('title'))

                <span class="help-block">

                    <strong>{{ $errors->first('title') }}</strong>

                </span>

            @endif

        </div>

    </div>
    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="address">Location </label>
        <div class="col-sm-6">

            <input type='text' class="form-control" id="Address" name='location' placeholder="Enter Location" value="<?php
            if (!empty($saloon) && $saloon['location'] != "")
            {
                echo $saloon["location"];
            }
            ?>" />
            @if ($errors->has('location') && $errors->has('latitude') && $errors->has('longitude'))
                <span class="help-block">
                    <strong>{{ $errors->first('location') }}</strong><br>
                </span>
            @endif
            <div class="form-group{{ $errors->has('latitude') && $errors->has('longitude') ? ' has-error' : '' }} col-sm-6">
                @if ($errors->has('latitude') && $errors->has('longitude'))
                        <span class="help-block">
                        <strong>Update your location</strong>
                        </span>
                @endif
            </div>

        </div>

    </div>

    <div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
        <label class="col-sm-1 control-label" for="address1"> <span class="text-red">&nbsp;&nbsp;</span></label>
        <div class="col-sm-6">
            <p>Drag Marker to your Location.</p>
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map" style="min-height:300px;" >
            </div>
        </div>
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="title">Description  <span class="text-red">*</span></label>
        <div class="col-sm-6">
             {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','rows'=>'10','cols'=>'80', 'id'=>'editor1']) !!}

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>

    </div>

    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="image">Saloon Logo<span class="text-red">*</span></label>

        <div class="col-sm-6">

            <div class="">

                {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}

            </div>
            <?php

            if (!empty($saloon->image) && $saloon->image != "") {

            ?>

            <br><img id="DisplayImage" src="{{ url($saloon->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >

            <?php

            }else{

                echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';

            } ?>

            @if ($errors->has('image'))

                <span class="help-block">

                        <strong>{{ $errors->first('image') }}</strong>

                    </span>

            @endif

        </div>

    </div>
    <div class="form-group{{ $errors->has('start_hour') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="title">Start Hour <span class="text-red">*</span></label>

        <div class="col-sm-6">

            <div class="input-group bootstrap-timepicker">
            @if(!empty($saloon->start_hour))
                <input type="text" class="form-control timepicker" name="start_hour" id="start_hour" value="{{$saloon->start_hour}}">
                <div class="input-group-addon">
                     <i class="fa fa-clock-o"></i>
                </div>
            @else
                 <input type="text" class="form-control timepicker" name="start_hour" id="start_hour" value="">
                <div class="input-group-addon">
                     <i class="fa fa-clock-o"></i>
                </div>
            @endif
            </div>
        </div>

    </div>

    <div class="form-group{{ $errors->has('end_hour') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="title">End Hour <span class="text-red">*</span></label>

        <div class="col-sm-6">
            <div class="input-group bootstrap-timepicker">
            @if(!empty($saloon->end_hour))
                <input type="text" class="form-control timepicker" name="end_hour" id="end_hour" value="{{$saloon->end_hour}}">
                <div class="input-group-addon">
                     <i class="fa fa-clock-o"></i>
                </div>
            @else
                 <input type="text" class="form-control timepicker" name="end_hour" id="end_hour" value="">
                <div class="input-group-addon">
                     <i class="fa fa-clock-o"></i>
                </div>
            @endif
            </div>
            @if ($errors->has('end_hour'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_hour') }}</strong>
                </span>
            @endif

        </div>

    </div>

    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>

        <div class="col-sm-6">

            @foreach (\App\saloon::$status as $key => $value)

                <label>

                    {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>

                </label>

            @endforeach



            @if ($errors->has('status'))

                <span class="help-block">

                 <strong>{{ $errors->first('status') }}</strong>

                </span>

            @endif

        </div>

    </div>

    <div class="form-group{{ $errors->has('global_discount') ? ' has-error' : '' }}">

        <label class="col-sm-1 control-label" for="role">Enable Discount <span class="text-red">*</span></label>

        <div class="col-sm-6">

            @foreach (\App\saloon::$global_discount as $key => $value)

                <label>

                    {!! Form::radio('global_discount', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>

                </label>

            @endforeach

            @if ($errors->has('global_discount'))

                <span class="help-block">

                 <strong>{{ $errors->first('global_discount') }}</strong>

                </span>

            @endif

        </div>

    </div>

    <script type="text/javascript">

    function AjaxUploadImage(obj,id){



        var file = obj.files[0];

        var imagefile = file.type;

        var match = ["image/jpeg", "image/png", "image/jpg"];

        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))

        {

            $('#previewing'+URL).attr('src', 'noimage.png');

            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            return false;

        } else{

            var reader = new FileReader();

            reader.onload = imageIsLoaded;

            reader.readAsDataURL(obj.files[0]);

        }

    }

    function imageIsLoaded(e) {



        $('#DisplayImage').css("display", "block");

        $('#DisplayImage').attr('src', e.target.result);

        $('#DisplayImage').attr('width', '150');



    };

</script>

<input type="hidden" name="latitude" id="latitude" value="<?php
if (!empty($saloon) && $saloon['latitude'] != "") {
    echo $saloon["latitude"];
}
?>">
<input type="hidden" name="longitude" id="longitude" value="<?php
if (!empty($saloon) && $saloon['longitude'] != "") {
    echo $saloon["longitude"];
}
?>">

<script type="text/javascript">
    var map;
    var markersArray = [];
            <?php
            if (!empty($saloon) && !empty($saloon["latitude"]) && !empty($saloon["longitude"])) {
                echo 'var myLatLng = {lat: ' . $saloon["latitude"] . ', lng: ' . $saloon["longitude"] . '};';
            } else {
                echo 'var myLatLng = {lat: 37.9642529, lng: -91.8318334};';
            }
            ?>
    var latitude;
    var longitude;
    var myLatLng;
    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 6
        });

        var marker = new google.maps.Marker({
            draggable: true,
            position: myLatLng,
            map: map,
        });

        var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
        google.maps.event.addListener(searchBox, 'places_changed', function() {
            searchBox.set('map', null);

            var places = searchBox.getPlaces();

            var bounds = new google.maps.LatLngBounds();
            var i, place;
            if(places[0] !== undefined) {

                place = places[0];

                console.log(place.geometry.location.lat() + ', ' + place.geometry.location.lng());

                $("#latitude").val(place.geometry.location.lat());
                $("#longitude").val(place.geometry.location.lng());

                marker.setPosition( place.geometry.location );
                map.panTo( place.geometry.location );
                $('#Address').val(place.formatted_address);

            }

            searchBox.set('map', map);
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            $("#latitude").val(event.latLng.lat());
            $("#longitude").val(event.latLng.lng());

            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#Address').val(results[0].formatted_address);
                    }
                }
            });
        });

/*
        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
            marker.setMap(null);
            $("#latitude").val(event.latLng.lat());
            $("#longitude").val(event.latLng.lng());

            var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#Address').val(results[0].formatted_address);
                    }
                }
            });
        });
*/
    }

    function clearMarkers() {
        setMapOnAll(null);
    }


    function placeMarker(location) {
        deleteOverlays();

        var marker = new google.maps.Marker({
            position: location,
            map: map
        });

        markersArray.push(marker);
    }

    function deleteOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9VE1DP_XUwvuN4e5sT5hFy8HZu-gZUAU&callback=initMap&libraries=places" async defer></script>

@section('jquery')
<script type="text/javascript">

    $("#start_hour").timepicker({
        showPeriod: true
    });

    $("#end_hour").timepicker({
        showPeriod: true
    });

</script>
@stop