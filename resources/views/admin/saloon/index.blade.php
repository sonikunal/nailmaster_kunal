@extends('admin.layouts.app')

@section('content')



    <div class="content-wrapper">

        <section class="content-header">

            <h1>

                {{$menu}}

            </h1>

            <ol class="breadcrumb">

                <li><a href="{{ url('admin/saloon') }}"><i class="fa fa-dashboard"></i> Saloon</a></li>

            </ol>

        </section>

        <section class="content">

            @include ('admin.error')

            <div id="responce" name="responce" class="alert alert-success" style="display: none">

            </div>

            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/saloon'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-sm-5 col-xs-12" style="padding-top: 5px">
                                <select name="type" style="width: 100%" class="select2 form-control">
                                    <option value="">Please Select</option>
                                    <option value="title" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='title') selected="selected" @endif>Title</option>
                                    
                                    <option value="start_hour" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='start_hour') selected="selected" @endif>Start Hour</option>

                                    <option value="end_hour" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='end_hour') selected="selected" @endif>End Hour</option>

                                    <option value="status" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='status') selected="selected" @endif>Status</option>
                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">

                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')

                                <a href="{{ url('admin/saloon/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>

                            @elseif( ! count($saloon) && \Illuminate\Support\Facades\Auth::user()->role == 'sub_admin')

                                <a href="{{ url('admin/saloon/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>

                            @endif

                        <a href="{{ url('admin/saloon') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>

                    </h3>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive">

                    <table class="table table-bordered table-striped" id="example2">

                        <thead>

                        <tr>

                            <th>Edit</th>

                            <th>Id</th>
                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                                <th>Owner</th>
                            @endif
                            <th>Saloon Name</th>
                            <th>Start Hour</th>
                            <th>End Hour</th>
                            <th>Image</th>    
                            <th>Status</th>
                            <th>Delete</th>

                        </tr>

                        </thead>

                        <tbody id="sortable">

                        @foreach ($saloon as $list)

                            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">

                                <td>

                                    <div class="btn-group-horizontal">

                                        {{ Form::open(array('url' => 'admin/saloon/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}

                                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit saloon" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>

                                        {{ Form::close() }}

                                    </div>

                                </td>

                                <td>{{ $list['id'] }}</td>

                                @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')
                                    <td>{{ $list['User']['email'] }}</td>
                                @endif

                                <td>{{$list['title']}}</td>

                                <td>{{$list['start_hour']}}</td>

                                <td>{{$list['end_hour']}}</td>

                                <td>

                                    @if($list['image']!="" && file_exists($list['image']))

                                        <img src="{{ url($list->image) }}" width="50">

                                    @endif

                                </td> 

                                <td>

                                    @if($list['status'] == 'active')

                                        <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >

                                            <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px" ><span class="ladda-label" >Active</span> </button>

                                        </div>

                                        <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >

                                            <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>

                                        </div>

                                    @endif

                                    @if($list['status'] == 'in-active')

                                        <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >

                                            <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>

                                        </div>

                                        <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >

                                            <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>

                                        </div>

                                    @endif

                                </td>



                                <td>

                                    <div class="btn-group-horizontal">

                                        <span data-toggle="tooltip" title="Delete saloon" data-trigger="hover">

                                            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>

                                        </span>


                                    </div>

                                </td>

                            </tr>



                            <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">

                                {{ Form::open(array('url' => 'admin/saloon/'.$list['id'], 'method' => 'delete','style'=>'display:inline')) }}

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                                <span aria-hidden="true">&times;</span></button>

                                            <h4 class="modal-title">Delete saloon</h4>

                                        </div>

                                        <div class="modal-body">

                                            <p>Are you sure you want to delete this saloon ?</p>

                                        </div>

                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>

                                            <button type="submit" class="btn btn-outline">Delete</button>

                                        </div>

                                    </div>

                                </div>

                                {{ Form::close() }}

                            </div>

                        @endforeach

                    </table>
                    <div style="text-align:right;float:right;"> @include('admin.pagination.limit_links', ['paginator' => $saloon])</div>

                </div>

            </div>

        </section>

    </div>

@endsection



<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">

<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>

<script>Ladda.bind( 'input[type=submit]' );</script>

<script type="text/javascript">

    $(document).ready(function(){

        $('.assign').click(function(){

            var user_id = $(this).attr('uid');

            var l = Ladda.create(this);

            l.start();

            $.ajax({

                url: '{{url('admin/saloon/assign')}}',

                type: "put",

                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},

                success: function(data){

                    l.stop();

                    $('#assign_remove_'+user_id).show();

                    $('#assign_add_'+user_id).hide();

                }

            });

        });

        $('.unassign').click(function(){

            var user_id = $(this).attr('ruid');

            var l = Ladda.create(this);

            l.start();

            $.ajax({

                url: '{{url('admin/saloon/unassign')}}',

                type: "put",

                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},

                success: function(data){

                    l.stop();

                    $('#assign_remove_'+user_id).hide();

                    $('#assign_add_'+user_id).show();

                }

            });

        });

    });

</script>





<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">

<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.css')}}">
<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/drag_drop/jquery-ui.js')}}"></script>
<script>

    function slideout() {
        setTimeout(function() {
            $("#responce").slideUp("slow", function() {
            });

        }, 3000);
    }

    $("#responce").hide();
    $( function() {
        $( "#sortable" ).sortable({opacity: 0.9, cursor: 'move', update: function() {
            var order = $(this).sortable("serialize") + '&update=update';
            $.get("{{url('admin/saloon/reorder')}}", order, function(theResponse) {
                $("#responce").html(theResponse);
                $("#responce").slideDown('slow');
                slideout();
            });
        }});
        $( "#sortable" ).disableSelection();
    } );



</script>

