<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="name">Full Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Full Name']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="email">Email <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'email']) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="inputPassword3">Password</label>
    <div class="col-sm-5">
        <input type="password" placeholder="Password" id="password" name="password" class="form-control" >
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label class="col-sm-1 control-label" for="inputPassword3">Confirm Password</label>
    <div class="col-sm-5">
        <input type="password" placeholder="Confirm password" id="password-confirm" name="password_confirmation" class="form-control" >
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
             <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Phone <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::number('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone']) !!}

        @if ($errors->has('phone'))

            <span class="help-block">

                <strong>{{ $errors->first('phone') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Address </label>

    <div class="col-sm-5">

        {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) !!}

        @if ($errors->has('address'))

            <span class="help-block">

                <strong>{{ $errors->first('address') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Country <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Enter Country']) !!}

        @if ($errors->has('country'))

            <span class="help-block">

                <strong>{{ $errors->first('country') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="image">Image<span class="text-red">*</span></label>

    <div class="col-sm-5">

        <div class="">


            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}

        </div>



        <?php

        if (!empty($user->image) && $user->image != "") {

        ?>

        <br><img id="DisplayImage" src="{{ url($user->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >

        <?php

        }else{

            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';

        } ?>



        @if ($errors->has('image'))

            <span class="help-block">

                    <strong>{{ $errors->first('image') }}</strong>

                </span>

        @endif

    </div>

</div>

<script>



    $("#image").fileinput({

        showUpload: false,

        showCaption: false,



        showPreview: false,

        showRemove: false,

        browseClass: "btn btn-primary btn-lg btn_new",

    });



    function AjaxUploadImage(obj,id){



        var file = obj.files[0];

        var imagefile = file.type;

        var match = ["image/jpeg", "image/png", "image/jpg"];

        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))

        {

            $('#previewing'+URL).attr('src', 'noimage.png');

            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            return false;

        } else{

            var reader = new FileReader();

            reader.onload = imageIsLoaded;

            reader.readAsDataURL(obj.files[0]);

        }

    }



    function imageIsLoaded(e) {



        $('#DisplayImage').css("display", "block");

        $('#DisplayImage').attr('src', e.target.result);

        $('#DisplayImage').attr('width', '150');



    };



</script>