{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="type_id">User Role<span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::select('role', \App\User::$user_role, null, ['id'=>'role', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('role'))
            <span class="help-block">
                <strong>{{ $errors->first('role') }}</strong>
            </span>
        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Name <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}

        @if ($errors->has('name'))

            <span class="help-block">

                <strong>{{ $errors->first('name') }}</strong>

            </span>

        @endif

    </div>

</div>



<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="email">Email <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) !!}

            @if ($errors->has('email'))

            <span class="help-block">

            <strong>{{ $errors->first('email') }}</strong>

            </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="password">Password <span class="text-red">*</span></label>

    <div class="col-sm-5">

        <input type="password" placeholder="Enter Password" autocomplete="off"  id="password" maxlength="10" name="password" class="form-control" >

        @if ($errors->has('password'))

            <span class="help-block">

                <strong>{{ $errors->first('password') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Phone <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone']) !!}

        @if ($errors->has('phone'))

            <span class="help-block">

                <strong>{{ $errors->first('phone') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Country <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::select('country', [''=>'Please select'] + $countries, !empty($country)?$country:null, ['id'=>'country', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}

        @if ($errors->has('country'))

            <span class="help-block">

                <strong>{{ $errors->first('country') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">State <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::select('state_id', $states, !empty($state_id)?$state_id:null, ['id'=>'state_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}

        @if ($errors->has('state_id'))

            <span class="help-block">

                <strong>{{ $errors->first('state_id') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">City <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::select('city_id', $cities, !empty($city_id)?$city_id:null, ['id'=>'city_id', 'class' => 'select2 form-control', 'style' => 'width: 100%']) !!}

        @if ($errors->has('city_id'))

            <span class="help-block">

                <strong>{{ $errors->first('city_id') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('landmark') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Landmark </label>

    <div class="col-sm-5">

        {!! Form::text('landmark', null, ['class' => 'form-control', 'placeholder' => 'Enter Landmark']) !!}

        @if ($errors->has('landmark'))

            <span class="help-block">

                <strong>{{ $errors->first('landmark') }}</strong>

            </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('addressline_1') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Address Line 1 <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('addressline_1', null, ['class' => 'form-control', 'placeholder' => 'Address Line 1']) !!}

        @if ($errors->has('addressline_1'))

            <span class="help-block">

                <strong>{{ $errors->first('addressline_1') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('addressline_2') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Address Line 2</label>

    <div class="col-sm-5">

        {!! Form::text('addressline_2', null, ['class' => 'form-control', 'placeholder' => 'Address Line 2']) !!}

        @if ($errors->has('addressline_2'))

            <span class="help-block">

                <strong>{{ $errors->first('addressline_2') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Zip Code</label>

    <div class="col-sm-5">

        {!! Form::text('zip_code', null, ['class' => 'form-control', 'placeholder' => 'Zip Code']) !!}

        @if ($errors->has('zip_code'))

            <span class="help-block">

                <strong>{{ $errors->first('zip_code') }}</strong>

            </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="image">Profile Pciture </label>

    <div class="col-sm-5">

        <div class="">

            {!! Form::file('image', ['class' => '', 'id'=> 'image', 'onChange'=>'AjaxUploadImage(this)']) !!}

        </div>

        <?php

        if (!empty($user->image) && $user->image != "") {

        ?>

        <br><img id="DisplayImage" src="{{ url($user->image) }}" name="img" id="img" width="150" style="padding-bottom:5px" >

        <?php

        }else{

            echo '<br><img id="DisplayImage" src="" width="150" style="display: none;"/>';

        } ?>



        @if ($errors->has('image'))

            <span class="help-block">

                    <strong>{{ $errors->first('image') }}</strong>

                </span>

        @endif

    </div>

</div>



<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>

    <div class="col-sm-5">

        @foreach (\App\User::$status as $key => $value)

            <label>

                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>

            </label>

        @endforeach



        @if ($errors->has('status'))

            <span class="help-block">

             <strong>{{ $errors->first('status') }}</strong>

            </span>

        @endif

    </div>

</div>

<script type="text/javascript">


    $("#image").fileinput({

        showUpload: false,

        showCaption: false,



        showPreview: false,

        showRemove: false,

        browseClass: "btn btn-primary btn-lg btn_new",

    });

    function AjaxUploadImage(obj,id){



        var file = obj.files[0];

        var imagefile = file.type;

        var match = ["image/jpeg", "image/png", "image/jpg"];

        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))

        {

            $('#previewing'+URL).attr('src', 'noimage.png');

            alert("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            //$("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");

            return false;

        } else{

            var reader = new FileReader();

            reader.onload = imageIsLoaded;

            reader.readAsDataURL(obj.files[0]);

        }

    }

    function imageIsLoaded(e) {



        $('#DisplayImage').css("display", "block");

        $('#DisplayImage').attr('src', e.target.result);

        $('#DisplayImage').attr('width', '150');



    };

</script>
@section('jquery')
    <script type="text/javascript">

        $(document).ready(function(){

            $("#country").change(function() {

                if ($(this).val() != '') {

                    $.ajax({
                        url: '{{ url('admin/users/get_states') }}/' + $(this).val(),
                        error: function () {
                        },
                        success: function (result) {
                            //alert(result);
                            $("#state_id").select2().empty();
                            $("#state_id").html(result);
                            $('#state_id').select2()
                        }
                    });

                } else {

                    $("#state_id").empty();
                    $("#state_id").html('');
                    $('#state_id').select2()

                }
            });

            $("#state_id").change(function() {

                if ($(this).val() != '') {

                    var cc = $('#country').val();

                    $.ajax({
                        url: '{{ url('admin/users/get_cities') }}/'+cc+'/'+$(this).val(),
                        error: function () {
                        },
                        success: function (result) {
                            //alert(result);
                            $("#city_id").select2().empty();
                            $("#city_id").html(result);
                            $('#city_id').select2()
                        }
                    });

                } else {

                    $("#city_id").empty();
                    $("#city_id").html('');
                    $('#city_id').select2()

                }
            });

        });

    </script>
@endsection