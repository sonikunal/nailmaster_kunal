{!! Form::hidden('redirects_to', URL::previous()) !!}


<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Title <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}

        @if ($errors->has('title'))

            <span class="help-block">

                <strong>{{ $errors->first('title') }}</strong>

            </span>

        @endif

    </div>

</div>

<div class="form-group{{ $errors->has('min_duration') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="title">Duration <span class="text-red">*</span></label>

    <div class="col-sm-5">

        {!! Form::text('min_duration', null, ['class' => 'form-control', 'placeholder' => 'Duration (Minutes)']) !!}

        @if ($errors->has('min_duration'))

            <span class="help-block">

                <strong>{{ $errors->first('min_duration') }}</strong>

            </span>

        @endif

    </div>

</div>


<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">

    <label class="col-sm-1 control-label" for="role">Status <span class="text-red">*</span></label>

    <div class="col-sm-5">

        @foreach (\App\Service_type::$status as $key => $value)

            <label>

                {!! Form::radio('status', $key, null, ['class' => 'flat-red']) !!} <span style="margin-right: 10px">{{ $value }}</span>

            </label>

        @endforeach



        @if ($errors->has('status'))

            <span class="help-block">

             <strong>{{ $errors->first('status') }}</strong>

            </span>

        @endif

    </div>

</div>
