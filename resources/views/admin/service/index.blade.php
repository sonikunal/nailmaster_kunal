@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" >

        <section class="content-header">

            <h1>

                {{$menu}}

            </h1>

            <ol class="breadcrumb">

                <li><a href="{{ url('admin/service') }}"><i class="fa fa-dashboard"></i> Service Type </a></li>

            </ol>

        </section>

        <section class="content">

            @include ('admin.error')

            <div id="responce" name="responce" class="alert alert-success" style="display: none">

            </div>
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-5">
                        {!! Form::open(['url' => url('admin/service'), 'method' => 'get', 'class' => 'form-horizontal','files'=>false]) !!}
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-sm-5 col-xs-12" style="padding-top: 5px">
                                <select name="type" style="width: 100%" class="select2 form-control" id="type">
                                    <option value="">Please Select</option>
                                    <option value="title" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='title') selected="selected" @endif>Title</option>
                                    
                                    <option value="status" @if (isset($_REQUEST['type']) && $_REQUEST['type']=='status') selected="selected" @endif>Status</option>
                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <span style="float:left; padding-top: 5px" class="col-md-4 col-sm-5 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-2 col-xs-12">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" name="submit" value="Search">
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-7">
                        <h3 class="box-title" style="float:right;">

                        <a href="{{ url('admin/service/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>

                        <a href="{{ url('admin/service') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>

                    </h3>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body table-responsive " id="itemlist">
                    @include('admin.service.table')
                </div>

            </div>

        </section>

    </div>

@endsection


<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">

<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>

<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>


<script>Ladda.bind( 'input[type=submit]' );</script>

<!-- ajax status code add custom js -->


<script type="text/javascript">
</script>
<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
         e.preventDefault();
         $('#load').append('<img style="position: absolute; width:150px; left: 469px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/load.gif') }}" />');

        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page) 
    {
        $.ajax({

            url : page

        }).done(function (data) {

            $("#itemlist").empty().html(data);
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true

            });

        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }

    $(document).on('click','#searchbtn',function(e){
         e.preventDefault();

         var search = $('#search').val();
         var type = $('#type').val();
         $('#load').append('<img style="position: absolute; width:150px; left: 469px;   top: 90px; z-index: 100000;" src="{{URL::asset('assets/dist/img/load.gif') }}" />');
         $.ajax({

                url: '{{url('admin/service')}}',

                type: "get",

                data: {'type': type,'search':search,'_token' : $('meta[name=_token]').attr('content')},

                success: function(data){
                    //alert(data);

                    $("#itemlist").empty().html(data);
                    $('#example2').DataTable({
                        "paging": false,
                        "lengthChange": true,
                        "searching": false,
                        "ordering": true,
                        "info": false,
                        "autoWidth": true

                    });
                }

            });
    });
</script>





