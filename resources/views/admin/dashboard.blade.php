@extends('admin.layouts.app')

@section('content')



<div class="content-wrapper">

    <section class="content-header">

        <h1>

            Dashboard

            <small>Control panel</small>

        </h1>

        <ol class="breadcrumb">

            <li class="active"><a href="{{url('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>

        </ol>

    </section>



    <section class="content">

        <div class="row">

            @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $total_user}}</h3>
                        <p>Total Users</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-th"></i>
                    </div>
                    <a href="{{ url('admin/users') }}" class="small-box-footer">View Users <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
        </div>
    </section>

</div>

@endsection



@section("jquery")

    <script src="{{ URL::asset('assets/plugins/chartjs/Chart.min.js')}}"></script>

    <script type="text/javascript">



        $(function (){



            var amountChartData = {

                labels: {!! json_encode($amount_chart_labels) !!},

                datasets: [

                    {

                        label: "Amount",

                        fillColor: "rgba(60,141,188,0.9)",

                        strokeColor: "rgba(60,141,188,0.8)",

                        pointColor: "#3b8bba",

                        pointStrokeColor: "rgba(60,141,188,1)",

                        pointHighlightFill: "#fff",

                        pointHighlightStroke: "rgba(60,141,188,1)",

                        data: {!! json_encode($amount_chart_data) !!}

                    }

                ]

            };







            var amountChartOptions = {

                //Boolean - If we should show the scale at all

                showScale: true,

                //Boolean - Whether grid lines are shown across the chart

                scaleShowGridLines: false,

                //String - Colour of the grid lines

                scaleGridLineColor: "rgba(0,0,0,.05)",

                //Number - Width of the grid lines

                scaleGridLineWidth: 1,

                //Boolean - Whether to show horizontal lines (except X axis)

                scaleShowHorizontalLines: true,

                //Boolean - Whether to show vertical lines (except Y axis)

                scaleShowVerticalLines: true,

                //Boolean - Whether the line is curved between points

                bezierCurve: true,

                //Number - Tension of the bezier curve between points

                bezierCurveTension: 0.3,

                //Boolean - Whether to show a dot for each point

                pointDot: false,

                //Number - Radius of each point dot in pixels

                pointDotRadius: 4,

                //Number - Pixel width of point dot stroke

                pointDotStrokeWidth: 1,

                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point

                pointHitDetectionRadius: 20,

                //Boolean - Whether to show a stroke for datasets

                datasetStroke: true,

                //Number - Pixel width of dataset stroke

                datasetStrokeWidth: 1,

                //Boolean - Whether to fill the dataset with a color

                datasetFill: false,

                //String - A legend template

                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",

                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container

                maintainAspectRatio: true,

                //Boolean - whether to make the chart responsive to window resizing

                responsive: true

            };





            var amountChartCanvas = $("#BusinessChart").get(0).getContext("2d");

            var lineChart = new Chart(amountChartCanvas);

            lineChart.Line(amountChartData, amountChartOptions);



        });



    </script>

@endsection