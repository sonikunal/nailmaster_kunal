<!DOCTYPE html>
<html lang="en">
<head>
    <title>Nail Master - Coming Soon</title>
    <meta charset="utf-8">
    <meta name="description" content="Nail Master - Coming Soon"/>

    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/reset.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/website/css/main.css') }}">

    <script src="{{ URL::asset('assets/website/js/jquery.js')}}"></script>
    <script src="{{ URL::asset('assets/website/js/TimeCircles.js')}}"></script>
    <script src="{{ URL::asset('assets/website/js/backstretch.js')}}"></script>
    <script src="{{ URL::asset('assets/website/js/main.js')}}"></script>
</head>
<body>

@yield('content')

</body>
</html>