<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login','api\AppUserController@login');
Route::post('logout','api\AppUserController@logout');
Route::post('register','api\AppUserController@register');
Route::post('get_profile','api\AppUserController@get_profile');
Route::post('profile_update','api\AppUserController@profile_update');
Route::post('forgot_password','api\AppUserController@forgot_password');
Route::post('get_saloons','api\AppUserController@get_saloons');