<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'admin/home'], function () {

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::auth();
    Route::get('/', 'admin\DashboardController@index');

    Route::get('dashboard', 'admin\DashboardController@index');

    /* USER MANAGEMENT */
    Route::put('users/assign', 'admin\UserController@assign');
    Route::put('users/unassign', 'admin\UserController@unassign');
    Route::get('users/get_states/{cc}', 'admin\UserController@get_states');
    Route::get('users/get_cities/{cc}/{st}', 'admin\UserController@get_cities');
    Route::resource('users', 'admin\UserController');
    Route::resource('profile_update', 'admin\ProfileupdateController');


    /* SALOON EMPLOYEE MANAGEMENT */
    Route::put('employees/assign', 'admin\EmployeeController@assign');
    Route::put('employees/unassign', 'admin\EmployeeController@unassign');
    Route::resource('employees', 'admin\EmployeeController');

     /* SALOON MANAGEMENT */
    Route::get('saloon/reorder', 'admin\SaloonController@reorder');
    Route::put('saloon/assign', 'admin\SaloonController@assign');
    Route::put('saloon/unassign', 'admin\SaloonController@unassign');
    Route::get('saloon/services/{id}', 'admin\SaloonController@services');
    Route::put('saloon/save_services/{id}', 'admin\SaloonController@save_services');
    Route::get('saloon/work_days/{id}', 'admin\SaloonController@work_days');
    Route::put('saloon/save_days/{id}', 'admin\SaloonController@save_days');
    Route::get('saloon/images/{id}', 'admin\SaloonController@images');
    Route::resource('saloon', 'admin\SaloonController');

     /* SERVICE TYPE MANAGEMENT */

    Route::get('service/reorder', 'admin\ServiceTypeController@reorder');
    Route::put('service/assign', 'admin\ServiceTypeController@assign');
    Route::put('service/unassign', 'admin\ServiceTypeController@unassign');
    Route::resource('service', 'admin\ServiceTypeController');


    /* BOOKING MANAGEMENT */
    Route::get('booking/{status}/{id}', 'admin\BookingController@update_status');
    Route::resource('booking', 'admin\BookingController');


    Auth::routes();

});

/*------------------------------------ROUTE FOR WEBSITE---------------------------------*/

Route::get('/', 'website\HomeController@index');
Route::post('register_user', 'website\RegisterUserController@register_user');
Route::get('register', 'website\RegisterUserController@index');
