<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('state_id')->after("country");
            $table->integer('city_id')->after("state_id");
            $table->string('suburb', 100)->after("city_id");
            $table->string('addressline_1', 255)->after("suburb");
            $table->string('addressline_2', 255)->after("addressline_1");
            $table->string('postal_code', 10)->after("addressline_2");
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
