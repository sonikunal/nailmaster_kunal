<?php

$factory('App\Device',[
    'name' =>$faker->sentence(),
    'default_name' => $faker->sentence(),
    'image' => 'resource/device/UpJwrF7yyddurx5GkQu5.png',
    'status' => 'active',
    'url' => $faker->url,
    'type' => 'room',
    'floor' => $faker->numberBetween(0,60)
]);

$factory('App\Timetable',[
    'room_id' => 'factory:App\Device',
    'title1' => $faker->sentence(),
    'title2' => $faker->sentence(),
    'venue1' => $faker->sentence(),
    'venue2' => $faker->sentence(),
    'event_start_time' => $faker->dateTime(),
    'event_end_time' => $faker->dateTime(),
    'available_start_time' => $faker->dateTime(),
    'available_end_time' => $faker->dateTime(),
    'image' => 'resource/device/UpJwrF7yyddurx5GkQu5.png',
    'image_status' => 'active',
    ]);
